# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

ZSH_THEME="dieter"

plugins=(
  git
  bundler
  osx
  rake
  rbenv
  ruby
)

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export PATH="$HOME/.rbenv/bin:$PATH"

source $ZSH/oh-my-zsh.sh

# Aliases
alias zshconfig="atom ~/.zshrc"
alias ohmyzsh="atom ~/.oh-my-zsh"
alias be='bundle exec'
alias lockscreen="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"
alias :fire:='🔥'
alias src="source ~/.zshrc"
alias specs="rspec spec"
eval $(thefuck --alias)

eval "$(rbenv init -)"

function wthr {
  if [ $# -eq 0 ]; then
    curl -4 http://wttr.in/boston;
  else
    curl -4 http://wttr.in/$1;
  fi; }
